package com.example.myapplication;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.myapplication.data.CeriRepository;

import java.util.List;

public class ListViewModel extends AndroidViewModel {

    private CeriRepository repository;
    private LiveData<List<Item>> allItems;

    public ListViewModel (Application application) {
        super(application);
        repository = CeriRepository.get(application);
        allItems = repository.getAllItems();
    }


    LiveData<List<Item>> getAllItems() {
        return allItems;
    }



    public Integer countItems(){
        return repository.countItems();
    }


    public void loadCollection() {
        repository.loadCollection();
    }



}
