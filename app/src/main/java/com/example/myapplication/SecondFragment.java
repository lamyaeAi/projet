package com.example.myapplication;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class SecondFragment extends Fragment {

    private DetailViewModel viewModel;
    private TextView textName;
    private EditText textBrand, textYear, textCategorie, textDescription;
    private ImageView imageView;

    private long itemId;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    @SuppressLint("WrongViewCast")
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        SecondFragmentArgs args = SecondFragmentArgs.fromBundle(getArguments());
        itemId = args.getItemNum();
        Log.d("TAG","item id = :"+itemId);
        viewModel.setItem(itemId);
        textName = view.findViewById(R.id.name);
        textBrand = view.findViewById(R.id.editText1);
        textCategorie = view.findViewById(R.id.editText2);
        textYear = view.findViewById(R.id.editText3);
        textDescription = view.findViewById(R.id.editText4);
        imageView=view.findViewById(R.id.imageView);
        // img = getView().findViewById(R.id.i);



        observerSetup();

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }

    private void observerSetup() {

        viewModel.getItem().observe(getViewLifecycleOwner(),

                item -> {

                    if (item != null) {

                        textName.setText(""+item.getName());
                        if (item.getBrand() != null)
                            textBrand.setText(""+item.getBrand());
                        if (item.getYear() != null)
                            textYear.setText(""+item.getBrand());
                        if (item.getCategories() != null)
                            textCategorie.setText(""+item.getCategories()+"");
                        if (item.getDescription() != null)
                            textDescription.setText((""+item.getDescription()));
                        imageView.setImageResource(R.drawable.pc1);
                    }

                });
    }
}
