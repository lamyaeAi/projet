package com.example.myapplication;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class FirstFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    ListViewModel viewModel;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_first, container, false);
        viewModel = new ViewModelProvider(this).get(ListViewModel.class);

        Integer countItems=viewModel.countItems();
        if(countItems==0) {
            viewModel.loadCollection();
        }
        recyclerView = view.findViewById(R.id.recycler_adapter);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setListViewModel(viewModel);
        observerSetup();
        return view;
    }

    public void observerSetup() {
        viewModel.getAllItems().observe(getViewLifecycleOwner(),
                items -> adapter.setItemList(items));

    }
}
