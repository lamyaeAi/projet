package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Item_table")
public class Item {

    public static final String TAG = Item.class.getSimpleName();
    public static final long ADD_ID = -1;

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="_id")
    private long id;

    public Item() {}

    public Item(String itemId) {
        this.itemID=itemId;

    }

    @NonNull
    @ColumnInfo(name="itemId")
    private String itemID;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @ColumnInfo(name="description")
    private String description;

    @ColumnInfo(name = "categories")
    private String categories;

    @ColumnInfo(name = "timeFrame")
    private String timeFrame;

    @ColumnInfo(name = "year")
    private Integer year;

    @ColumnInfo(name = "brand")
    private String brand;

    @ColumnInfo(name = "technicalDetails")
    private String technicalDetails;

    @ColumnInfo(name = "pictures")
    private String pictures;

    /*@TypeConverters(DataConverter.class)
    @ColumnInfo(name = "pictures")
     private HashMap<String,String> pictures;*/

    @ColumnInfo(name = "working")
    private Boolean working;

    public Boolean isWorking() {
        return working;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }

    public String getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(String technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    @NonNull
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @NonNull
    public String getItemID() {
        return itemID;
    }

    public void setItemID(@NonNull String itemID) {
        this.itemID = itemID;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

}
