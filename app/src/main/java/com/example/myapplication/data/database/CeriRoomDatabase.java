package com.example.myapplication.data.database;

import android.content.Context;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;


import com.example.myapplication.Item;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Item.class }, version = 1, exportSchema = false)
public abstract class CeriRoomDatabase extends RoomDatabase {

    private static final String TAG = CeriRoomDatabase.class.getSimpleName();
    public abstract ItemDao ItemDao();

    private static CeriRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static CeriRoomDatabase getDatabase(final Context context) {

        if (INSTANCE == null) {
            synchronized (CeriRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            CeriRoomDatabase.class,"Ceri_database")
                            //.addCallback(sRoomDatabaseCallback)
                            .build();

                    Log.d("app","Dataase DONE! ");
                }
            }
        }
        return INSTANCE;
    }

}
