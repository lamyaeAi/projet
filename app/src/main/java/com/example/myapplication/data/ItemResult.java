package com.example.myapplication.data;

import android.util.Log;

import com.example.myapplication.Item;

public class ItemResult {

    static void transferInfo(ItemResponse response, Item item) {
        Log.d("app","its result");
        item.setName(response.name);
        item.setDescription(response.description);
        item.setCategories(""+response.categories);
        item.setBrand(response.brand);
        item.setYear(response.year);
        item.setTimeFrame(""+response.timeFrame);
        item.setTechnicalDetails(""+response.technicalDetails);
        item.setWorking(response.working);


    }

}
