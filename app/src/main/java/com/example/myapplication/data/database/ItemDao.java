package com.example.myapplication.data.database;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.myapplication.Item;
import com.example.myapplication.data.database.ItemDao;

import java.util.List;

@Dao
public interface ItemDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertItem(Item item);

    @Query("SELECT count(*) from item_table")
    Integer countItems();

    @Query("SELECT * from item_table")
    LiveData<List<Item>> getAllItems();

    @Query("SELECT * FROM item_table WHERE _id = :id")
    Item getItem(double id);

    @Query("DELETE FROM item_table")
    void deleteAllItems();

    @Query("select itemId from item_table where itemId= :id")
    String find(String id);
}
