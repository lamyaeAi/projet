package com.example.myapplication.data;

import androidx.room.TypeConverters;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemResponse {
    public final String name=null;
    public final String description=null;
    public final List<String> categories=null;
    public final Integer year=null;
    public final List<Integer> timeFrame=null;
    public final String brand=null;
    public final List<String> technicalDetails=null;
    public final Boolean working=null;


}
