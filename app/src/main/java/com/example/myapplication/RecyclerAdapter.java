package com.example.myapplication;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Item> itemList;
    private ListViewModel listViewModel;


    private static int View_holder1 = 1;
    private static int View_holder2 = 2;


    public void setListViewModel(ListViewModel viewModel) {
        listViewModel = viewModel;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == View_holder1) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card0, parent, false);
            return new ViewHolder1(view);

        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card1, parent, false);
            return new ViewHolder2(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == View_holder1) {
            ((ViewHolder1) holder).itemTitle.setText(itemList.get(position).getName());
            if(itemList.get(position).getName()==null){
                ((ViewHolder1) holder).itemTitle.setText(itemList.get(position).getBrand());
            }
            ((ViewHolder1)holder).itemImage.setImageResource(R.drawable.pc1);
            ((ViewHolder1) holder).itemCategorie.setText(itemList.get(position).getCategories());
        } else {
            ((ViewHolder2) holder).itemTitle.setText(itemList.get(position).getName());
            if(itemList.get(position).getName()==null){
                ((ViewHolder2) holder).itemTitle.setText(itemList.get(position).getBrand());
            }
            ((ViewHolder2)holder).itemImage.setImageResource(R.drawable.pc1);
            ((ViewHolder2) holder).itemCategorie.setText(itemList.get(position).getCategories());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 ==0) {
            return View_holder2;

        } else {
            return View_holder1;
        }
    }
    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }
    public void setItemList(List<Item> items) {
        itemList = items;
        notifyDataSetChanged();
    }

    class ViewHolder1 extends RecyclerView.ViewHolder {

        private TextView itemTitle;
        private TextView itemCategorie;
        private ImageView itemImage;

        ViewHolder1(@NonNull View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemCategorie = itemView.findViewById(R.id.item_categorie);
            itemImage=itemView.findViewById(R.id.item_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    long id = RecyclerAdapter.this.itemList.get((int)getAdapterPosition()).getId();
                    FirstFragmentDirections.ActionFirstFragmentToSecondFragment action = FirstFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setItemNum(id);
                    Log.d("app","id :"+id+"id 2:"+action.getItemNum());
                    Navigation.findNavController(v).navigate(action);


                }
            });
        }

    }
    class ViewHolder2 extends RecyclerView.ViewHolder {

        private TextView itemTitle;
        private TextView itemCategorie;
        private ImageView itemImage;

        ViewHolder2(@NonNull View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemCategorie = itemView.findViewById(R.id.item_categorie);
            itemImage=itemView.findViewById(R.id.item_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    long id = RecyclerAdapter.this.itemList.get((int)getAdapterPosition()).getId();

                    FirstFragmentDirections.ActionFirstFragmentToSecondFragment action = FirstFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setItemNum(id);
                    Log.d("app","id :"+id+"id 2:"+action.getItemNum());
                    Navigation.findNavController(v).navigate(action);

                }
            });
        }
    }
}
